interface Rectangle {
    width : number;
    heigth : number;
}

interface ColoredRectangle extends Rectangle{
    color: string;
}

const rectangle : Rectangle = {
    width : 16,
    heigth : 9
} 
console.log(rectangle)

const coloredRectangle : ColoredRectangle = {
    width : 16,
    heigth : 9,
    color : "red"
}

console.log(coloredRectangle)